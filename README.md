<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## laravel crud con adminlte

Contiene : 
1. Login y registro
2. crud Presonas (datatable, actualización de personas y opciones usando ajax)
3. crud Configuración

## Instalación
1. Importar archivo SQL a su motor de Base de Datos MySQL
2. Configurar Archivo .env dependiendo su servidor de Mysql
3. Composer Install
3. npm Install
4. php artisan migrate
5. npm run dev
6. php artisan key:generate
7. composer dump-autoload
