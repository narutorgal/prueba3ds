<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('admin/persona', [App\Http\Controllers\PersonaController::class, 'index'])->name('persona');
    Route::get('admin/persona/sublista', [App\Http\Controllers\PersonaController::class, 'sublista'])->name('sublista');
    Route::get('admin/persona/get-data', [App\Http\Controllers\PersonaController::class, 'getData']);
    Route::post('admin/persona/save', [App\Http\Controllers\PersonaController::class, 'store']);
    Route::post('admin/persona/destroy/{id}', [App\Http\Controllers\PersonaController::class, 'destroy']);
    Route::get('admin/lista', [App\Http\Controllers\ListaController::class, 'index'])->name('lista');
    Route::get('admin/lista/get-data', [App\Http\Controllers\ListaController::class, 'getData']);
    Route::post('admin/lista/save', [App\Http\Controllers\ListaController::class, 'store']);
    Route::post('admin/lista/destroy/{id}', [App\Http\Controllers\ListaController::class, 'destroy']);
    Route::get('admin/sublista', [App\Http\Controllers\SublistaController::class, 'index'])->name('category');
    Route::get('admin/sublista/get-data', [App\Http\Controllers\SublistaController::class, 'getData']);
    Route::post('admin/sublista/save', [App\Http\Controllers\SublistaController::class, 'store']);
    Route::post('admin/sublista/destroy/{id}', [App\Http\Controllers\SublistaController::class, 'destroy']);

});
