<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Lista;
use Illuminate\Http\Request;

class ListaController extends Controller
{
    public function getData() {
        $data = Lista::orderBy('id')->get();

        return datatables()->of($data)
        ->addIndexColumn()
        ->make(true);
    }


    public function store(Request $req){
        $id = $req->id?:0;

        if(!$id) {
            $validated = $req->validate([
                'lista' => 'required|unique:lista|max:255',
            ]);
        }

        $data_input = $req->all();

        if($id) {
            $data_input['updated_at'] = date('Y-m-d H:i:s');
        } else {
            $data_input['created_at'] = date('Y-m-d H:i:s');
        }

        $lista = Lista::updateOrCreate(['id' => $id], $data_input);

        if ($lista) {
			$message = array();
            $message['message'] = 'Registro guardado con Exito';

            return response()->json($message)->setStatusCode(200);
		}else{

			$message = array();
            $message['message'] = 'Error al guardar el registro';

            return response()->json($message)->setStatusCode(400);
		}
	}

	public function destroy($id){
        $lista = Lista::where('id', $id)->first();

        if ($lista->delete()) {
			$message = array();
            $message['message'] = 'Registro Eliminado con exito';

            return response()->json($message)->setStatusCode(200);
		}else{

			$message = array();
            $message['message'] = 'Error al eliminar el registro';

            return response()->json($message)->setStatusCode(400);
		}
	}
}
