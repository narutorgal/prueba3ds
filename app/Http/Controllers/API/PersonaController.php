<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Persona;
use App\Models\Lista;
use App\Models\Sublista;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use File;

class PersonaController extends Controller
{
    public function getData() {
        $data = Persona::where(['estado' => '1'])->orderBy('id')->get();
        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }


    public function store(Request $req){
        $id = $req->id?:0;

        $validated = $req->validate([
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'id_genero' => 'required',
            'id_tipo' => 'required',
            'cedula' => 'required|numeric|unique|max:20',
            'telefono' => 'required|numeric|unique|max:10',
            'email' => 'required|unique|max:255'
        ]);

        $data_input = $req->all();
        if($id) {
            $data_input['updated_at'] = date('Y-m-d H:i:s');
            $data_input['updated_by'] = auth()->user()->id;
        } else {
            $data_input['creaed_by'] = auth()->user()->id;
            $data_input['created_at'] = date('Y-m-d H:i:s');
        }

        $data_input['creaed_by'] = auth()->user()->id;

        $person = Persona::updateOrCreate(['id' => $id], $data_input);

        if ($person) {
			$message = array();
            $message['message'] = 'Registro almacenado con Exito';

            return response()->json($message)->setStatusCode(200);
		}else{

			$message = array();
            $message['message'] = 'Error en guardar el registro';

            return response()->json($message)->setStatusCode(400);
		}
	}

	public function destroy($id){
        $person = Persona::where('id', $id)->first();

        if ($person->delete()) {
			$message = array();
            $message['message'] = 'Registro eliminado con exito';

            return response()->json($message)->setStatusCode(200);
		}else{

			$message = array();
            $message['message'] = 'Error al eliminar el registro';

            return response()->json($message)->setStatusCode(400);
		}
	}
}
