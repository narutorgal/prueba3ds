<?php

namespace App\Http\Controllers;

use App\Models\Lista;
use Illuminate\Http\Request;

class ListaController extends Controller
{
    public function index()
    {
        return view('pages/lista/index');
    }

    public function getData()
    {
        $data = Lista::where(['estado' => '1'])->orderBy('lista')->get();
        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }


    public function store(Request $req){
        $id = $req->id?:0;

        $validated = $req->validate([
            'lista' => 'required|max:255',
        ]);

        $data_input = $req->all();
        if($id) {
            $data_input['updated_at'] = date('Y-m-d H:i:s');
        } else {
            $data_input['created_at'] = date('Y-m-d H:i:s');
        }

        $lista1 = Lista::updateOrCreate(['id' => $id], $data_input);

        if ($lista1) {
			$message = array();
            $message['message'] = 'Registro Almacenado con Exito';

            return response()->json($message)->setStatusCode(200);
		}else{

			$message = array();
            $message['message'] = 'Error al guardar el registro';

            return response()->json($message)->setStatusCode(400);
		}
	}

    public function destroy($id)
    {
        $lista = Lista::where('id', $id)->first();

        if ($lista->delete()) {
            $message = array();
            $message['message'] = 'Registro eliminado con Exito';

            return response()->json($message)->setStatusCode(200);
        } else {

            $message = array();
            $message['message'] = 'Error al eliminar el registro';

            return response()->json($message)->setStatusCode(400);
        }
    }
}
