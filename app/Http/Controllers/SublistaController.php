<?php

namespace App\Http\Controllers;

use App\Models\Lista;
use App\Models\Sublista;
use Illuminate\Http\Request;

class SublistaController extends Controller
{
    public function index()
    {
        $gen = Lista::where(['estado' => '1'])->orderBy('id')->get();
        return view('pages/sublista/index', compact('gen'));
    }

    public function getData()
    {
        $data = Sublista::where(['estado' => '1'])->orderBy('id')->get();
        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }


    public function store(Request $req)
    {
        $id = $req->id ?: 0;

        $validated = $req->validate([
            'sublista_nom' => 'required|max:255',
        ]);

        $data_input = $req->all();
        if ($id) {
            $data_input['updated_at'] = date('Y-m-d H:i:s');
        } else {
            $data_input['created_at'] = date('Y-m-d H:i:s');
        }

        $lista = Sublista::updateOrCreate(['id' => $id], $data_input);

        if ($lista) {
            $message = array();
            $message['message'] = 'Registro Almacenado con Exito';

            return response()->json($message)->setStatusCode(200);
        } else {

            $message = array();
            $message['message'] = 'Error al guardar el registro';

            return response()->json($message)->setStatusCode(400);
        }
    }

    public function destroy($id)
    {
        $sublista = Sublista::where('id', $id)->first();

        if ($sublista->delete()) {
            $message = array();
            $message['message'] = 'Registro eliminado con Exito';

            return response()->json($message)->setStatusCode(200);
        } else {

            $message = array();
            $message['message'] = 'Error al eliminar el registro';

            return response()->json($message)->setStatusCode(400);
        }
    }
}
