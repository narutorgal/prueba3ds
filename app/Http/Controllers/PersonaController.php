<?php

namespace App\Http\Controllers;

use App\Models\Persona;
use App\Models\Lista;
use App\Models\Sublista;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use File;

class PersonaController extends Controller
{
    public function index() {
        $tip = Sublista::where(['estado'=>'1','id_lista'=>'1'])->orderBy('id')->get();
        $gen = Sublista::where(['estado'=>'1','id_lista'=>'2'])->orderBy('id')->get();
        return view('pages/persona/index', compact('tip'), compact('gen'));
    }

    public function getData() {
        $data = Persona::with(['sublista_gen'],['sublista_tip'])->latest()->get();

        return datatables()->of($data)
        ->addColumn('sublista_nom', function ($data) {
            return isset($data->sublista->sublista_nom)? $data->sublista->sublista_nom : '';
        })
        ->addIndexColumn()
        ->make(true);
    }


    public function store(Request $req){
        $id = $req->id?:0;

        $validated = $req->validate([
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'id_genero' => 'required',
            'id_tipo' => 'required',
            'cedula' => 'required|',
            'telefono' => 'required|',
            'email' => 'required|email|max:255',
        ]);

        $data_input = $req->all();
        if($id) {
            $data_input['updated_at'] = date('Y-m-d H:i:s');
            $data_input['updated_by'] = auth()->user()->id;
        } else {
            $data_input['creaed_by'] = auth()->user()->id;
            $data_input['created_at'] = date('Y-m-d H:i:s');
        }

        $persona = Persona::updateOrCreate(['id' => $id], $data_input);

        if ($persona) {
			$message = array();
            $message['message'] = 'Registro Almacenado con Exito';

            return response()->json($message)->setStatusCode(200);
		}else{

			$message = array();
            $message['message'] = 'Error al guardar el registro';

            return response()->json($message)->setStatusCode(400);
		}
	}

	public function destroy($id){
        $persona = Persona::where('id', $id)->first();

        if ($persona->delete()) {
			$message = array();
            $message['message'] = 'Registro Eliminado con Exito';

            return response()->json($message)->setStatusCode(200);
		}else{

			$message = array();
            $message['message'] = 'Error al eliminar el registro';

            return response()->json($message)->setStatusCode(400);
		}
	}


    public function sublista() {
        $persona = Persona::with('sublista')->latest()->
                    where('created_by', auth()->user()->id)->get();

        return view('pages/sublista/index', compact('persona'));
    }
}
