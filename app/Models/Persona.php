<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    protected $table = 'persona';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function sublista_gen()
    {
        return $this->belongsTo(Sublista::class);
    }
    public function sublista_tip()
    {
        return $this->belongsTo(Sublista::class);
    }
}
