<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sublista extends Model
{
    use HasFactory;
    protected $table = 'sublista';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
