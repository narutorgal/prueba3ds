@extends('adminlte::page')

@section('title', 'Persona')

@section('content_header')
    <h1>Persona</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <button type="button" class="btn btn-primary mt-3 ml-3"
                        onclick="$('#InputModal').modal('show');resetForm()">+ Crear</button>
                </div>
                <div class="table-responsive mt-4">
                    <table id="persona" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>id_genero</th>
                                <th>id_tipo</th>
                                <th>Cedula</th>
                                <th>Telefono</th>
                                <th>Email</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="InputModal" aria-labelledby="InputModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="InputModalLabel">Crear</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('admin/persona/save') }}" id="formPersona">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" id="id" name="id">
                            <label for="nombre" class="col-sm-3 col-form-label">Nombre</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" autocomplete="off" id="nombre" name="nombre"
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="apellido" class="col-sm-3 col-form-label">Apellido</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" autocomplete="off" id="apellido" name="apellido"
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_genero" class="col-sm-3 col-form-label">Genero</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <select class="form-control" id="id_genero">
                                        <option value="" selected disabled>Seleccionar</option>
                                        @foreach ($gen as $genero)
                                            <option value="{{ $genero->id }}">{{ $genero->sublista_nom }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_tipo" class="col-sm-3 col-form-label">Tipo Identificacion</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <select class="form-control" id="id_tipo">
                                        <option value="" selected disabled>Seleccionar</option>
                                        @foreach ($tip as $tipo)
                                            <option value="{{ $tipo->id }}">{{ $tipo->sublista_nom }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cedula" class="col-sm-3 col-form-label">Cedula</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="20" autocomplete="off" id="cedula"
                                    name="cedula" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="telefono" class="col-sm-3 col-form-label">Telefono</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="10" autocomplete="off" id="telefono"
                                    name="telefono" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Correo Electrónico</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" autocomplete="off" id="email" name="email"
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" onclick="savePersona()" data-dismiss="modal" id="submitPersona"
                            class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        .modal {
            overflow-y: auto;
        }
    </style>
@stop

@section('js')
    <script type="text/javascript">
        $(".select2").select2();
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            loadList();

        });

        function loadList() {
            const page_url = '{{ url('admin/persona/get-data') }}';

            $.fn.dataTable.ext.errMode = 'ignore';
            var table = $('#persona').DataTable({
                processing: true,
                serverSide: true,
                "bDestroy": true,
                ajax: {
                    url: page_url,
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nombre',
                        name: 'nombre'
                    },
                    {
                        data: 'apellido',
                        name: 'apellido'
                    },
                    {
                        data: 'id_genero',
                        name: 'id_genero'
                    },
                    {
                        data: 'id_tipo',
                        name: 'id_tipo'
                    },
                    {
                        data: 'cedula',
                        name: 'cedula'
                    },
                    {
                        data: 'telefono',
                        name: 'telefono'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    
                    {
                        "data": null,
                        "sortable": false,
                        render: function(data, type, row, meta) {
                            var result = '<a class="btn btn-success btn-sm" \
                                                data-id = ' + row.id + ' \
                                                data-nombre = ' + row.nombre + ' \
                                                data-apellido = ' + row.apellido + ' \
                                                data-id_genero = ' + row.id_genero + ' \
                                                data-id_tipo = ' + row.id_tipo + ' \
                                                data-cedula = ' + row.cedula + ' \
                                                data-telefono = ' + row.telefono + ' \
                                                data-email = ' + row.email + ' \
                                            onclick="editPersona(this)" data-toggle="modal" data-target="#InputModal"><i class="fa fa-pencil"></i> Actualizar</a>&nbsp;';
                            result += '<a class="btn btn-warning btn-sm" onclick="destroy(' + row.id +
                                ')"><i class="fa fa-pencil"></i> Eliminar</a>';
                            return result;
                        }
                    }
                ],
                responsive: true,
                oLanguage: {
                    sLengthMenu: "_MENU_",
                    sSearch: ""
                },
                aLengthMenu: [
                    [4, 10, 15, 20],
                    [4, 10, 15, 20]
                ],
                order: [
                    [1, "asc"]
                ],
                pageLength: 10,
                buttons: [],
                initComplete: function(settings, json) {
                    $(".dt-buttons .btn").removeClass("btn-secondary")
                },
                drawCallback: function(settings) {
                    console.log(settings.json);
                }
            });

        }

        function editPersona(e) {
            $('#id').val($(e).data('id'));
            $('#nombre').val($(e).data('nombre'));
            $('#apellido').val($(e).data('apellido'));
            $('#id_genero').val($(e).data('id_genero')).trigger('change');
            $('#id_tipo').val($(e).data('id_tipo')).trigger('change');
            $('#cedula').val($(e).data('cedula'));
            $('#telefono').val($(e).data('telefono'));
            $('#email').val($(e).data('email'));
            $('.alert').hide();
        }

        function savePersona() {
            let id = document.getElementById('id').value;
            let nombre = document.getElementById('nombre').value;
            let apellido = document.getElementById('apellido').value;
            let id_genero = $('#id_genero').val();
            let id_tipo = $('#id_tipo').val();
            let cedula = document.getElementById('cedula').value;
            let telefono = document.getElementById('telefono').value;
            let email = document.getElementById('email').value;

            var url = "{{ url('admin/persona/save') }}";

            if (nombre == '') {
                Swal.fire("Error!", "Nombre es requerido", "error");
            } else {
                // swalLoading();
                document.getElementById("submitPersona").disabled = true;
                var form_data = new FormData();
                form_data.append('id', id);
                form_data.append('nombre', nombre);
                form_data.append('apellido', apellido);
                form_data.append('id_genero', id_genero);
                form_data.append('id_tipo', id_tipo);
                form_data.append('cedula', cedula);
                form_data.append('telefono', telefono);
                form_data.append('email', email);

                $.ajax({
                    type: "POST",
                    url: url,
                    beforeSend: function(xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: form_data,
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(result) {
                        document.getElementById("submitPersona").disabled = false;

                        $('#InputModal').modal('hide');
                        Swal.fire("Exito!", result.message, "success");
                        loadList();
                    },
                    error: function(xhr, status, error) {
                        Swal.fire("Error!", JSON.stringify(xhr.responseJSON.errors), "error");
                        document.getElementById("submitPersona").disabled = false;
                    },

                });
            }
        }

        function destroy(id) {
            var url = "{{ url('admin/persona/destroy') }}" + "/" + id;
            Swal.fire({
                title: `Esta Seguro?`,
                text: ` Eliminado es permanente!`,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        beforeSend: function(xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: "json",
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(result) {
                            Swal.fire("Success!", result.message, "success");
                            loadList();
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseJSON.message);
                        },

                    });
                } else {

                }
            })
        }

        function resetForm() {
            $('#id').val('');
            $('#nombre').val('');
            $('#apellido').val('');
            $('#id_genero').val('').trigger('change');
            $('#id_tipo').val('').trigger('change');
            $('#cedula').val('');
            $('#telefono').val('');
            $('#email').val('');
            $('.alert').hide();
            $('#formPersona').trigger("reset");
        }
    </script>
    <script>
        $('.numeral-mask').each(function(index, ele) {
            var cleaveCustom = new Cleave(ele, {
                numeral: true,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop
