@extends('adminlte::page')

@section('title', 'Listas')

@section('content_header')
    <h1>Listas</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <button type="button" class="btn btn-primary mt-3 ml-3"
                        onclick="$('#InputModal').modal('show');resetForm()">+ Crear</button>
                </div>
                <div class="table-responsive mt-4">
                    <table id="listas" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Lista</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="InputModal" aria-labelledby="InputModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="InputModalLabel">Crear</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('admin/lista/save') }}" id="formLista">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" id="id" name="id">
                            <label for="lista" class="col-sm-3 col-form-label">Nombre Lista</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" autocomplete="off" id="lista" name="lista"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" onclick="saveLista()" data-dismiss="modal" id="submitLista"
                            class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            loadList();

        });

        function loadList() {
            const page_url = '{{ url('admin/lista/get-data') }}';

            $.fn.dataTable.ext.errMode = 'ignore';
            var table = $('#listas').DataTable({
                processing: true,
                serverSide: true,
                "bDestroy": true,
                ajax: {
                    url: page_url,
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'lista',
                        name: 'lista'
                    },
                    {
                        "data": null,
                        "sortable": false,
                        render: function(data, type, row, meta) {
                            var result = '<a class="btn btn-success btn-sm" \
                                            data-id = ' + row.id + ' \
                                            data-lista = ' + row.lista +
                                ' \
                                        onclick="editLista(this)" data-toggle="modal" data-target="#InputModal"><i class="fa fa-pencil"></i> Actualizar</a>&nbsp;';
                            result += '<a class="btn btn-warning btn-sm" onclick="destroy(' + row.id +
                                ')"><i class="fa fa-pencil"></i> Eliminar</a>';
                            return result;
                        }
                    }
                ],
                responsive: true,
                oLanguage: {
                    sLengthMenu: "_MENU_",
                    sSearch: ""
                },
                aLengthMenu: [
                    [4, 10, 15, 20],
                    [4, 10, 15, 20]
                ],
                order: [
                    [1, "asc"]
                ],
                pageLength: 10,
                buttons: [],
                initComplete: function(settings, json) {
                    $(".dt-buttons .btn").removeClass("btn-secondary")
                },
                drawCallback: function(settings) {
                    console.log(settings.json);
                }
            });

        }

        function editLista(e) {
            $('#id').val($(e).data('id'));
            $('#lista1').val($(e).data('lista1'));
            $('.alert').hide();
        }

        function saveLista() {
            let id = document.getElementById('id').value;
            let lista = document.getElementById('lista').value;
            var url = "{{ url('admin/lista/save') }}";
            if (lista == '') {
                Swal.fire("Error!", "Nombre es requerido", "error");
            } else {
                // swalLoading();
                document.getElementById("submitLista").disabled = true;
                var form_data = new FormData();
                form_data.append('id', id);
                form_data.append('lista', lista);

                $.ajax({
                    type: "POST",
                    url: url,
                    beforeSend: function(xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: form_data,
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(result) {
                        document.getElementById("submitLista").disabled = false;

                        $('#InputModal').modal('hide');
                        Swal.fire("Exito!", result.message, "success");
                        loadList();
                    },
                    error: function(xhr, status, error) {
                        Swal.fire("Error!", JSON.stringify(xhr.responseJSON.errors), "error");
                        document.getElementById("submitLista").disabled = false;
                    },

                });
            }
        }

        function destroy(id) {
            var url = "{{ url('admin/lista/destroy') }}" + "/" + id;
            Swal.fire({
                title: `Esta Seguro?`,
                text: ` el eliminado es permanente!`,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        beforeSend: function(xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: "json",
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(result) {
                            Swal.fire("Success!", result.message, "success");
                            loadList();
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseJSON.message);
                        },

                    });
                } else {

                }
            })
        }

        function resetForm() {
            $('#id').val('');
            $('#lista1').val('');
            $('.alert').hide();
            $('#formLista').trigger("reset");
        }
    </script>
@stop
