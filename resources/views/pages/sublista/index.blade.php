@extends('adminlte::page')

@section('title', 'Sublistas')

@section('content_header')
    <h1>Opciones</h1>
@stop

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <button type="button" class="btn btn-primary mt-3 ml-3"
                        onclick="$('#InputModal').modal('show');resetForm()">+ Crear</button>
                </div>
                <div class="table-responsive mt-4">
                    <table id="sublistas" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre Opción</th>
                                <th>Tipo Opción</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="InputModal" aria-labelledby="InputModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="InputModalLabel">Crear</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('admin/sublista/save') }}" id="formSublista">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" id="id" name="id">
                            <label for="sublista_nom" class="col-sm-3 col-form-label">Nombre Opción</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" autocomplete="off" id="sublista_nom" name="sublista_nom"
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_lista" class="col-sm-3 col-form-label">Tipo de Opción</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <select class="form-control" id="id_lista">
                                        <option value="" selected disabled>Seleccionar</option>
                                        @foreach ($gen as $genero)
                                            <option value="{{ $genero->id }}">{{ $genero->lista }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" onclick="saveSublista()" data-dismiss="modal" id="submitSublista"
                            class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        .modal {
            overflow-y: auto;
        }
    </style>
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        loadList();

    });

        function loadList() {
            const page_url = '{{ url('admin/sublista/get-data') }}';

            $.fn.dataTable.ext.errMode = 'ignore';
            var table = $('#sublistas').DataTable({
                processing: true,
                serverSide: true,
                "bDestroy": true,
                ajax: {
                    url: page_url,
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'sublista_nom',
                        name: 'sublista_nom'
                    },
                    {
                        data: 'id_lista',
                        name: 'id_lista'
                    },
                    {
                        "data": null,
                        "sortable": false,
                        render: function(data, type, row, meta) {
                            var result = '<a class="btn btn-success btn-sm" \
                                                data-id = ' + row.id + ' \
                                                data-sublista_nom = ' + row.sublista_nom + ' \
                                                data-id_lista = ' + row.id_lista + ' \
                                            onclick="editSublista(this)" data-toggle="modal" data-target="#InputModal"><i class="fa fa-pencil"></i> Actualizar</a>&nbsp;';
                            result += '<a class="btn btn-warning btn-sm" onclick="destroy(' + row.id +
                                ')"><i class="fa fa-pencil"></i> Eliminar</a>';
                            return result;
                        }
                    }
                ],
                responsive: true,
                oLanguage: {
                    sLengthMenu: "_MENU_",
                    sSearch: ""
                },
                aLengthMenu: [
                    [4, 10, 15, 20],
                    [4, 10, 15, 20]
                ],
                order: [
                    [1, "asc"]
                ],
                pageLength: 10,
                buttons: [],
                initComplete: function(settings, json) {
                    $(".dt-buttons .btn").removeClass("btn-secondary")
                },
                drawCallback: function(settings) {
                    console.log(settings.json);
                }
            });

        }

        function editSublista(e) {
            $('#id').val($(e).data('id'));
            $('#sublista_nom').val($(e).data('sublista_nom'));
            $('#id_lista').val($(e).data('id_lista')).trigger('change');
            $('.alert').hide();
        }

        function saveSublista() {
            let id = document.getElementById('id').value;
            let sublista_nom = document.getElementById('sublista_nom').value;
            let id_lista = $('#id_lista').val();
            var url = "{{ url('admin/sublista/save') }}";

            if (sublista_nom == '') {
                Swal.fire("Error!", "Nombre es requerido", "error");
            } else {
                // swalLoading();
                document.getElementById("submitSublista").disabled = true;
                var form_data = new FormData();
                form_data.append('id', id);
                form_data.append('sublista_nom', sublista_nom);
                form_data.append('id_lista', id_lista);
                $.ajax({
                    type: "POST",
                    url: url,
                    beforeSend: function(xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: form_data,
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(result) {
                        document.getElementById("submitSublista").disabled = false;

                        $('#InputModal').modal('hide');
                        Swal.fire("Exito!", result.message, "success");
                        loadList();
                    },
                    error: function(xhr, status, error) {
                        Swal.fire("Error!", JSON.stringify(xhr.responseJSON.errors), "error");
                        document.getElementById("submitSublista").disabled = false;
                    },

                });
            }
        }

        function destroy(id) {
            var url = "{{ url('admin/sublista/destroy') }}" + "/" + id;
            Swal.fire({
                title: `Esta Seguro?`,
                text: ` Eliminado es permanente!`,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        beforeSend: function(xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: "json",
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(result) {
                            Swal.fire("Success!", result.message, "success");
                            loadList();
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseJSON.message);
                        },

                    });
                } else {

                }
            })
        }

        function resetForm() {
            $('#id').val('');
            $('#sublista_nom').val('');
            $('#id_lista').val('').trigger('change');
            $('.alert').hide();
            $('#formSublista').trigger("reset");
        }
    </script>
    <script>
        $('.numeral-mask').each(function(index, ele) {
            var cleaveCustom = new Cleave(ele, {
                numeral: true,
                numeralDecimalMark: ',',
                delimiter: '.'
            });
        });
    </script>
@stop
