/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : prueba_laravel

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 14/07/2022 23:44:22
*/
CREATE DATABASE prueba_laravel;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lista
-- ----------------------------
DROP TABLE IF EXISTS `lista`;
CREATE TABLE `lista`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `lista` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `estado` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT 'Activo = 1\r\nInactivo = 0',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lista
-- ----------------------------
INSERT INTO `lista` VALUES (1, 'Tipo Documento', '1', NULL, NULL);
INSERT INTO `lista` VALUES (2, 'Genero', '1', NULL, NULL);
INSERT INTO `lista` VALUES (5, 'Tipo de Sangre', '1', '2022-07-14 22:59:38', '2022-07-14 22:59:38');

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `apellido` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id_genero` int(20) NOT NULL,
  `id_tipo` int(20) NOT NULL,
  `cedula` int(20) NOT NULL,
  `telefono` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `estado` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT 'Activo = 1\r\nInactivo = 0',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_sublista_tipo`(`id_tipo`) USING BTREE,
  INDEX `fk_sublista_genero`(`id_genero`) USING BTREE,
  UNIQUE INDEX `cedula`(`cedula`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  CONSTRAINT `fk_sublista_genero` FOREIGN KEY (`id_genero`) REFERENCES `sublista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sublista_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `sublista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES (1, 'ergrge regre', 'asdas', 6, 1, 123456789, 321654987, 'fewf@asdas.com', '2022-07-14 22:32:52', NULL, '2022-07-14 22:32:52', 1, '1');
INSERT INTO `persona` VALUES (2, 'Michael', 'Vanegas', 5, 1, 1070969028, 2147483647, 'maic.364@gmail.com', '2022-07-14 21:55:10', NULL, '2022-07-14 21:55:10', NULL, '1');

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` int(11) NOT NULL,
  `user_id` int(20) NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_activity` int(20) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sessions
-- ----------------------------

-- ----------------------------
-- Table structure for sublista
-- ----------------------------
DROP TABLE IF EXISTS `sublista`;
CREATE TABLE `sublista`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_lista` int(20) NOT NULL,
  `sublista_nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `estado` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT 'Activo = 1\r\nInactivo = 0',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_lista_sublista`(`id_lista`) USING BTREE,
  CONSTRAINT `fk_lista_sublista` FOREIGN KEY (`id_lista`) REFERENCES `lista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sublista
-- ----------------------------
INSERT INTO `sublista` VALUES (1, 1, 'Cedula de Ciudadania', '1', NULL, NULL);
INSERT INTO `sublista` VALUES (2, 1, 'Cedula de Extranjería', '1', NULL, NULL);
INSERT INTO `sublista` VALUES (3, 1, 'NIT', '1', NULL, '2022-07-14 23:15:21');
INSERT INTO `sublista` VALUES (4, 1, 'Pasaporte', '1', NULL, NULL);
INSERT INTO `sublista` VALUES (5, 2, 'Masculino', '1', NULL, NULL);
INSERT INTO `sublista` VALUES (6, 2, 'Femenino', '1', NULL, NULL);
INSERT INTO `sublista` VALUES (7, 2, 'Prefiero no Decir', '1', NULL, NULL);
INSERT INTO `sublista` VALUES (8, 5, 'O+', '1', '2022-07-14 23:15:29', '2022-07-14 23:15:29');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT 'Activo = 1\r\nInactivo = 0',
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'michael', '$2y$10$IbzEge9BwTouPSLj9R/n8eeST4h6pYB5ERzRD7vL9x0t5wjIbLBIu', 'maic.364@gmail.com', '1', '2022-07-14 14:44:42', '2022-07-14 14:44:42');
INSERT INTO `users` VALUES (2, 'Ana Rojas', '$2y$10$m.DzyZ4DcUbwkyJd0iUfc.iiMiUrCMdiHeHuXQX/WIyjR/Xdk9I.O', 'sharitperez@gmail.com', '1', '2022-07-14 23:43:40', '2022-07-14 23:43:40');

SET FOREIGN_KEY_CHECKS = 1;
